import { Command, flags } from '@oclif/command'
import { args } from '@oclif/parser';
import { cli } from 'cli-ux'

export default class Filter extends Command {
  static description = 'filter ELK Logs'

  static flags = {
    // flag with a value (-e, --environment=VALUE)
    environment: flags.string({ char: 'e', description: 'environment name ', required: true }),
    // flag with a value (-a, --app=VALUE)
    app: flags.string({ char: 'a', description: 'app name', default: '' }),
    // flag with a value (-c, --name=VALUE)
    name: flags.string({ char: 'c', description: 'image name', default: '' }),
    // flag with a value (-t, --name=VALUE)
    id: flags.string({ char: 't', description: 'container ID', default: '' }),
    // flag with a value (-l, --size=VALUE)
    size: flags.string({ char: 's', description: 'result size(limit)', default: '1000' }),
    help: flags.help({ char: 'h' }),
    // flag with no value (-f, --force)
    force: flags.boolean({ char: 'f' }),
  }

  static args = [
    {
      name: 'url',
      default: 'https://b046f28d7d414ecb8709f8c9c125a626.eu-west-1.aws.found.io:9243'
    },
    {
      name: 'username', default: 'elastic'
    },
    {
      name: 'password', default: 'xLF5GxUlbXFdgqpwF5rNCjel'
    }
  ]

  async run() {
    const { args, flags } = this.parse(Filter)

    const { Client } = require('@elastic/elasticsearch')
    const client = new Client({
      node: args.url,
      auth: {
        username: args.username,
        password: args.password
      }
    });

    // callback API
    // Let's search!
    try {

      var { body } = await client.search({
        body: {
          "size": flags.size,
          "query": {
            "bool": {
              "must": {
                "match":
                {
                  "docker.container.labels.environment": flags.environment
                }
              },
              "should": [
                {
                  "match": {
                    "docker.container.labels.app": flags.app
                  }
                },
                {
                  "match": {
                    "container.image.name": flags.name
                  }
                },
                {
                  "match": {
                    "container.id": flags.id
                  }
                }
              ]
            }
          }
        }
      });

      // (easy to work)
      let result = body.hits.hits;
      let data: { id: any; environment: any; app: any; name: any; deployer: any; timestamp: any }[] = [];
      result.forEach(function (item: { [x: string]: any; }, key: any) {

        data.push({
          id: item['_source'].container.id,
          environment: item['_source'].docker.container.labels.environment,
          app: item['_source'].docker.container.labels.app,
          name: item['_source'].container.image.name,
          deployer: item['_source'].docker.container.labels.deployer,
          timestamp: item['_source']['@timestamp']
        });
      });

      // build table
      cli.table(data, {
        container_id: {
          get: row => row.id
        },
        environment: {
          get: row => row.environment
        },
        app: {
          get: row => row.app
        },
        container_name: {
          get: row => row.name
        },
        deployer: {
          get: row => row.deployer
        },
        timestamp: {
          get: row => row.timestamp
        }
      }, {
        printLine: this.log,
        ...flags, // parsed flags
      });

    } catch (err) {
      console.log(err);
    }
  }
}
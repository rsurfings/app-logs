app-logs
========


[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/app-logs.svg)](https://npmjs.org/package/app-logs)
[![Downloads/week](https://img.shields.io/npm/dw/app-logs.svg)](https://npmjs.org/package/app-logs)
[![License](https://img.shields.io/npm/l/app-logs.svg)](https://github.com/ronaldo/app-logs/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @rsurfings/app-logs
$ app:logs COMMAND
running command...
$ app:logs (-v|--version|version)
@rsurfings/app-logs/2.0.4 linux-x64 node-v10.16.0
$ app:logs --help [COMMAND]
USAGE
  $ app:logs COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`app:logs filter [URL] [USERNAME] [PASSWORD]`](#applogs-filter-url-username-password)
* [`app:logs help [COMMAND]`](#applogs-help-command)

## `app:logs filter [URL] [USERNAME] [PASSWORD]`

filter ELK Logs

```
USAGE
  $ app:logs filter [URL] [USERNAME] [PASSWORD]

OPTIONS
  -a, --app=app                  app name
  -c, --name=name                image name
  -e, --environment=environment  (required) environment name
  -f, --force
  -h, --help                     show CLI help
  -s, --size=size                [default: 1000] result size(limit)
  -t, --id=id                    container ID
```

_See code: [src/commands/filter.ts](https://github.com/rsurfings/app-logs/blob/v2.0.4/src/commands/filter.ts)_

## `app:logs help [COMMAND]`

display help for app:logs

```
USAGE
  $ app:logs help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.2/src/commands/help.ts)_
<!-- commandsstop -->
